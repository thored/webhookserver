# webhookserver

#### 项目介绍
WebHookServer
本项目是本人为了方便自己个人服务器上项目做部署用的，写的很烂，有什么不对的欢迎指出给予改优。

#### 软件架构
采用了koa2搭建服务做了多项目部署的应用

#### 安装教程

1. clone 拉取代码
2. cd webhookserver
3. npm i

#### 使用说明

1. cp config.js.bk config.js
2. cp router.js.bk router.js
3. 配置项目地址，修改config.js文件内项目的路径，预先创建production、development文件夹
4. 添加项目路由，修改router.js文件内路由，一个项目一条路由，path项目路由地址，可随意命名，确保唯一性。token 密码,git 托管平台（看controller文件夹内的js文件），leng 开发语言（目前只有nodejs,php）
5. pm2 守护 node app.js
6. 可以使用nginx做反向代理

