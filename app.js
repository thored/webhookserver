'use strict';
const fs = require('fs');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Router = require('koa-router')();
const config = require('./config');
const routerArray = require('./router');
const app = new Koa();
const gitClass = {};

// 这里可以用sync是因为启动时只运行一次，不存在性能问题:
var files = fs.readdirSync(__dirname + '/controller');

// 过滤出.js文件:
var js_files = files.filter(f => {
  return f.endsWith('.js');
});
// 处理加载js
for (const filename of js_files) {
  gitClass[filename.split('.')[0]] = require(`./controller/${filename}`);
}
// console.log(js_files.indexOf('gitee2.js'));
// 处理post-data数据
app.use(bodyParser());
// 初始化路由
app.use(async (ctx, next) => {
  for (const routerObj of routerArray) {
    if (gitClass.hasOwnProperty(routerObj.git)) {
      ctx.routerobj = routerObj;
      Router.post(routerObj.path, gitClass[routerObj.git]['initck']);
    }
  }
  await next();
});

app.use(Router.routes()).use(Router.allowedMethods());
app.listen(config.port, () => {
  console.log(`starting at port ${config.port}`);
});
