'use strict';
const fs = require('fs');
const exec = require('child_process').exec;

module.exports = {
  execCommand(command, options, cb) {
    return new Promise((resolve, reject) => {
      const opt = options || {};
      exec(command, opt, function(err, stdout, stderr) {
        // if (err) console.log(err);
        resolve(stdout);
      });
    });
  },
  // 转驼峰命名发  push_hooks   =>   PushHooks
  humpNamed(str) {
    return str
      .toLowerCase()
      .replace(/(^|_)[a-z]/g, L => L.toUpperCase())
      .replace('_', '');
  },
  // 定义检测目录权限
  fsExistsSync(pathdir) {
    try {
      fs.accessSync(pathdir, fs.F_OK);
    } catch (e) {
      return false;
    }
    return true;
  }
};
