'use strict';
const comm = require('../comm');
const config = require('../config');
// 设定制定分之才能呢运行代码
const gitbaranchck = ['develop', 'master'];
const giteeIsProductionPath = {
  master: 'production',
  develop: 'development'
};
const gitalb = {
  // headerToken: 'x-gitlab-token',
  async initck(ctx, next) {
    // console.log(ctx);
    console.log('is gitlab');

    // 验证ToKen密码
    console.log(ctx.request);
    ctx.body = 'Done! gitlab';
  },
  validationToken(ctx, next) {
    return true;
  },
  PushHook(err, stdout, stderr) {
    if (err) return err;
    // console.log(err);
    console.log(stdout);
    // console.log(stderr);
    return stdout;
  }
};

module.exports = {
  async initck(ctx) {
    let postData = ctx.request.body;
    // 获取项目名称
    let project_name = postData.project.name;
    // 获取分支名称
    let branch_name = postData.ref.split('/')[2];
    // 获取git地址
    let git_url = postData.project.git_ssh_url;

    // 验证密码、hook钩子
    if (
      ctx.routerobj.token !== ctx.request.header['x-gitlab-token'] ||
      !ctx.request.header.hasOwnProperty('x-gitlab-event')
    ) {
      // throw new Error(`参数不正确`);
      ctx.body = '参数不正确';
      return;
    }
    // hook 是否存在
    const hookName = ctx.request.header['x-gitlab-event'].replace(' ', '');
    if (!gitalb.hasOwnProperty(hookName)) {
      // throw new Error(`系统钩子错误`);
      ctx.body = '系统钩子错误';
      return;
    }
    // 检验分支
    if (giteeIsProductionPath.hasOwnProperty(branch_name)) {
      // 获取目录
      let rootpath = config[giteeIsProductionPath[branch_name]] + '/';
      // 定义命令语句
      let command = ``;
      // 定义项目脚本预言
      const leng = function(leng) {
        if (leng == 'php') {
          return '&& composer install';
        }
        if (leng == 'nodejs') {
          return '&& npm install';
        }
      };
      let lengcomm = leng(ctx.routerobj.leng);
      // 检测项目目录是否存在
      if (!comm.fsExistsSync(rootpath + project_name)) {
        // 创建目录，clone项目
        command = `cd ${rootpath} && git clone -b ${branch_name} ${git_url} && cd ${project_name} ${lengcomm}`;
      } else {
        command = `cd ${projectPath} && git pull ${lengcomm}`;
      }

      console.log(command);
      let donestr = '';
      // 执行命令语句
      try {
        donestr = await comm.execCommand(command, {}, gitalb[hookName]);
        console.log(donestr);
      } catch (error) {
        console.log(error);
        donestr = error;
      }

      ctx.body = donestr;
      return;
    } else {
      ctx.body = postData.ref + '提交分支不允许操作';
      return;
    }
  }
};
