'use strict';

const comm = require('../comm');
const config = require('../config');

const giteeIsProductionPath = {
  merge_request_hooks: 'production',
  push_hooks: 'development' // Push / Tag Push 钩子.
};

// 设定制定分之才能呢运行代码
const gitbaranchck = ['refs/heads/develop', 'refs/heads/master'];

const gitee = {
  PushHooks(err, stdout, stderr) {
    if (err) return err;
    // console.log(err);
    console.log(stdout);
    // console.log(stderr);
    return stdout;
  },
  MergeRequestHooks(err) {
    console.log(err);
    return err;
  }
};

module.exports = {
  async initck(ctx) {
    let postData = ctx.request.body;
    // 检验分支
    if (gitbaranchck.indexOf(postData.ref) < 0) {
      ctx.body = postData.ref + '提交分支不允许操作';
      console.log(postData.ref + '提交分支不允许操作');
      return;
    }

    // 验证密码、hook钩子
    if (
      ctx.routerobj.token !== postData.password ||
      !postData.hasOwnProperty('hook_name')
    ) {
      // throw new Error(`参数不正确`);
      ctx.body = '参数不正确';
      return;
    }

    // hook 是否存在
    const hookName = comm.humpNamed(postData.hook_name);
    if (!gitee.hasOwnProperty(hookName)) {
      // throw new Error(`系统钩子错误`);
      ctx.body = '系统钩子错误';
      return;
    }
    let donestr = '';
    if (giteeIsProductionPath.hasOwnProperty(postData.hook_name)) {
      let command = `cd ${config[giteeIsProductionPath[postData.hook_name]]}${
        ctx.routerobj.path
      } && git pull`;
      console.log('command:' + command);
      try {
        donestr = await comm.execCommand(command, {}, gitee[hookName]);
        console.log(donestr);
      } catch (error) {
        console.log(error);
        donestr = error;
      }
    } else {
      donestr = 'hook error!';
    }
    ctx.body = donestr;
  }
};
